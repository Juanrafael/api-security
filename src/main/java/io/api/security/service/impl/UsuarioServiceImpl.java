package io.api.security.service.impl;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.api.security.dto.UsuarioDTO;
import io.api.security.repository.UsuarioRepo;
import io.api.security.service.UsuarioService;


@Service
public class UsuarioServiceImpl implements UsuarioService{
    
    @Autowired
    private UsuarioRepo usuarioRepo;
    
    public List<UsuarioDTO> getUsers(){
        
        return usuarioRepo.findAll().stream()
        //.map(usuario-> mapper.map(usuario, UsuarioDTO.class))
        .map(usuario-> UsuarioDTO.builder()
            .id(usuario.getId())
            .password(usuario.getPassword())
            .estado(usuario.getEstado())
            .usuario(usuario.getNombreUsuario())
            .build())
        .collect(Collectors.toList());
    }
}
