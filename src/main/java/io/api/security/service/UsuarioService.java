package io.api.security.service;

import java.util.List;

import io.api.security.dto.UsuarioDTO;


public interface UsuarioService {
    public List<UsuarioDTO> getUsers();
}
