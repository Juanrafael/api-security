package io.api.security.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.api.security.dto.UsuarioDTO;
import io.api.security.service.UsuarioService;
import java.util.List;
@RestController
public class AuthController {
    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/users")
    public ResponseEntity<List<UsuarioDTO>> getUsers(){
        return new ResponseEntity(usuarioService.getUsers(),HttpStatus.OK);
    }
}
