package io.api.security.enums;

public enum RolNombre {
    ROLE_ADMIN, ROLE_USER
}
