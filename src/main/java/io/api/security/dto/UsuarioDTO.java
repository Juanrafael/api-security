package io.api.security.dto;  

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UsuarioDTO {
    private int id;
    private String estado;
    private String password;
    private String usuario;
}
