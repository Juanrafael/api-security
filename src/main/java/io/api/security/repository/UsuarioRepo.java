package io.api.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.api.security.entity.UsuarioDAO;





@Repository
public interface UsuarioRepo extends JpaRepository<UsuarioDAO,Integer>{
    @Query(value = "SELECT * FROM usuario u", nativeQuery=true)
    List<UsuarioDAO> findAll();
    
}
