package io.api.security.entity;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


import lombok.Getter;
import lombok.Setter;
@Entity
@Table(name="usuario")
@Getter
@Setter
public class UsuarioDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name = "estado")
    private String estado;

    @Column(name = "password")
    private String password;
    @NotNull
    @Column(name = "nombreUsuario")
    private String nombreUsuario;
    
    @Column(name = "email")
    private String email;
    
    @NotNull
    @ManyToMany
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "id_usuario"),
    inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles = new HashSet<>();

    
}
